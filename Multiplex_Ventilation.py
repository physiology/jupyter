import ipywidgets as widgets
import time
import threading
import numpy as np
import bqplot as bq

from pulse.cdm.engine import eSwitch
from pulse.cdm.mechanical_ventilator import eDriverWaveform
from pulse.cdm.mechanical_ventilator_actions import SEMechanicalVentilatorConfiguration
from pulse.cdm.scalars import FrequencyUnit, PressureUnit, TimeUnit
from pulse.study.multiplex_ventilation.engine import MVEngine
from pulse.study.bind.MultiplexVentilation_pb2 import  *

class PlotData(object):
    __slots__ = ['p0_y', 'p1_y', 'y_min', 'y_max', '_start']
    def __init__(self):
        self._start = True
        
    def update(self, y0, y1):
        if self._start:
            self._start = False
            self.p0_y = np.linspace(y0-(abs(y0)*0.01), y0, 500)
            self.p1_y = np.linspace(y1-(abs(y1)*0.01), y1, 500)
            self.y_min = min(y0, y1)
            self.y_max = max(y0, y1)
        #Sampling at 1s below, so I can hard code x delta here to +1
        self.p0_y = np.append(self.p0_y, y0)
        self.p0_y = np.delete(self.p0_y, 0)
 
        self.p1_y = np.append(self.p1_y, y1)
        self.p1_y = np.delete(self.p1_y, 0)
        
        my = min(y0, y1)
        if my < self.y_min:
            self.y_min = my
        mx = max(y0, y1)
        if mx > self.y_max:
            self.y_max = mx

class MVSimExplorer(object):

    def __init__(self):
    
        self.pulse = MVEngine("./data/multiplex_ventilation/MVEngine.log", False, "./data/")
        self.setup_pulse = True
        self.update_plots = False
        self.waiting_to_start = True
        # Create and start the thread and wait for the user to make an engine
        self.thread = threading.Thread(target=self.pulse_loop)
        self.running = True
        self.thread.start()
        
        self.plots = {}
        self.plots["AirwayFlow_L_Per_min"] = PlotData()
        self.plots["AirwayPressure_cmH2O"] = PlotData()
        self.plots["ArterialCarbonDioxidePartialPressure_mmHg"] = PlotData()
        self.plots["ArterialOxygenPartialPressure_mmHg"] = PlotData()
        self.plots["HorowitzIndex_mmHg"] = PlotData()
        self.plots["EndTidalCarbonDioxidePressure_mmHg"] = PlotData()
        self.plots["MeanAirwayPressure_cmH2O"] = PlotData()
        self.plots["OxygenationIndex"] = PlotData()
        self.plots["OxygenSaturation"] = PlotData()
        self.plots["OxygenSaturationIndex_mmHg"] = PlotData()
        self.plots["SFRatio"] = PlotData()
        self.plots["TidalVolume_mL"] = PlotData()
        self.plots["TotalLungVolume_mL"] = PlotData()
        self.active_plot = "TotalLungVolume_mL"
        self.time_s = np.linspace(0, 1, 500)
        
        self.results = SimulationData()
        self.patient_a_ctrls = dict()
        self.patient_b_ctrls = dict()
        self.ventilator_ctrls = dict()
        
        # scales
        self.x_sc = bq.LinearScale()
        self.y_sc = bq.LinearScale()
        # axis
        self.x_ax = bq.Axis(
            label='Time (s)',
            scale=self.x_sc
        )
        self.y_ax = bq.Axis(
            scale=self.y_sc, 
            orientation='vertical'
        )
        # Lines
        self.p0_line = bq.Lines(
            scales={'x':self.x_sc, 'y':self.y_sc},
            stroke_width=3, colors=['blue']
        )
        self.p1_line = bq.Lines(
            scales={'x':self.x_sc, 'y':self.y_sc},
            stroke_width=3, colors=['green']
        )
        # Figure
        self.fig = bq.Figure(
            layout=widgets.Layout(width='720px', height='480px', justify_content='center'),
            axes=[self.x_ax, self.y_ax],
            marks=[self.p0_line, self.p1_line],
            fig_margin=dict(top=10, bottom=40, left=50, right=10)
        )
        #self.fig.animation_duration = 30
        
        self.update_ventilator = False
        self.ventilator = SEMechanicalVentilatorConfiguration()
        self.v_cfg =  self.ventilator.get_settings()
        self.v_cfg.set_connection(eSwitch.On)
        self.v_cfg.set_inspiration_waveform(eDriverWaveform.Square);
        self.v_cfg.set_expiration_waveform(eDriverWaveform.Square);
        
        self._generateUI()
        
    def run(self):
        display(self.UI)

    def _generateUI(self):

        patient_a_params = self._generate_patient_UI('A', self.patient_a_ctrls)
        patient_a_params.layout = widgets.Layout(margin='0 0 5px 0', border='3px solid #2d60a8')
        patient_b_params = self._generate_patient_UI('B', self.patient_b_ctrls)
        patient_b_params.layout = widgets.Layout(margin='5px 0 0 0', border='3px solid #269132')
        
        # Defaults per patient
        self.patient_a_ctrls['Compliance (mL/cmH20)'].value=25
        self.patient_a_ctrls['Resistance (cmH2O s/L)'].value=5
        self.patient_a_ctrls['Diffusion Impairment Factor'].value=0.7
        self.patient_b_ctrls['Compliance (mL/cmH20)'].value=40
        self.patient_b_ctrls['Resistance (cmH2O s/L)'].value=6
        self.patient_b_ctrls['Diffusion Impairment Factor'].value=0.8
        
        vent_settings = self._generate_ventilator_UI()
        
        patient_initial_conditions = widgets.VBox(
            [
                patient_a_params,
                patient_b_params
            ]
        )
        patient_initial_conditions.layout.justify_content = 'space-between'
        
        inputs = widgets.HBox(
            [
                patient_initial_conditions,
                vent_settings
            ]
        )
        inputs.layout.justify_content = 'space-between'
        
        self.simulate = widgets.Button(
            description='Simulate',
            disabled=False,
            button_style='',
            tooltip='Toggle Pulse',
            icon='',
            #layout=widgets.Layout(padding='25px 10px 0px 25px')
        )
        self.simulate.on_click(self.start_pulse)
        
        self.reset = widgets.Button(
            description='Reset',
            disabled=True,
            button_style='',
            tooltip='Reset Notebook',
            icon='',
            #layout=widgets.Layout(padding='25px 10px 0px 25px')
        )
        self.reset.on_click(self.reset_pulse)
        
        self.plot_selection=widgets.Dropdown(
            options=self.plots.keys(),
            value=self.active_plot,
            description='Plot :',
            disabled=False,
        )
        self.plot_selection.observe(self.change_plot)
        
        buttons = widgets.HBox(
            [
                self.simulate,
                self.reset,
                self.plot_selection
            ]
        )
        buttons.layout.justify_content = 'center'
        buttons.layout.justify_content = 'space-between'
        
        self.UI = widgets.VBox(
            [
                inputs,
                self.fig,
                buttons
            ]
        )

    def _generate_patient_UI(self, name, ctrls):

        heading_label = widgets.Label('Patient '+name)
        heading = widgets.HBox(
            [
                heading_label
            ]
        )
        heading.layout.justify_content = 'center'

        slider_layout = widgets.Layout(width='150px')
        ctrls['Compliance (mL/cmH20)'] = widgets.IntSlider(
                            value=10,
                            min=10,
                            max=50,
                            step=1,
                            disabled=False,
                            continuous_update=False,
                            orientation='horizontal',
                            readout=True,
                            readout_format='d',
                            layout=slider_layout
                        )
        ctrls['Resistance (cmH2O s/L)'] = widgets.FloatSlider(
                            value=5,
                            min=1.5,
                            max=30,
                            step=0.5,
                            disabled=False,
                            continuous_update=False,
                            orientation='horizontal',
                            readout=True,
                            readout_format='.1f',
                            layout=slider_layout
                        )
        ctrls['Diffusion Impairment Factor'] = widgets.FloatSlider(
                            value=0,
                            min=0,
                            max=1,
                            step=0.1,
                            disabled=False,
                            continuous_update=False,
                            orientation='horizontal',
                            readout=True,
                            readout_format='.1f',
                            layout=slider_layout
                            )

        # construct the UI
        label_layout = widgets.Layout(width='175px', padding='0px 0px 0px 5px')
        labels = widgets.VBox([
            widgets.Label(l, layout=label_layout) for l in ctrls.keys()
        ])

        settings = widgets.HBox([
            labels,
            widgets.VBox(
                list(ctrls.values())
            )
        ])

        patient = widgets.VBox([heading, settings])
        
        return patient

    def _generate_ventilator_UI(self):\

        heading_label = widgets.Label('Ventilator Settings')
        vent_settings_heading = widgets.HBox(
            [
                heading_label
            ]
        )
        vent_settings_heading.layout.justify_content = 'center'

        slider_layout = widgets.Layout(width='225px')
        self.ventilator_ctrls = {
                'RespirationRate (bpm)' : widgets.IntSlider(
                                value=20,
                                min=10,
                                max=30,
                                step=1,
                                disabled=False,
                                continuous_update=False,
                                orientation='horizontal',
                                readout=True,
                                readout_format='d',
                                layout=slider_layout
                            ),
                'I:E Ratio' : widgets.FloatSlider(
                                value=0.5,
                                min=0.25,
                                max=3,
                                step=0.01,
                                disabled=False,
                                continuous_update=False,
                                orientation='horizontal',
                                readout=True,
                                readout_format='.2f',
                                layout=slider_layout
                            ),
                'PIP (cmH2O)' : widgets.FloatSlider(
                                value=25,
                                min=15.0,
                                max=65.0,
                                step=0.5,
                                disabled=False,
                                continuous_update=False,
                                orientation='horizontal',
                                readout=True,
                                readout_format='.1f',
                                layout=slider_layout
                ),
                'PEEP (cmH2O)': widgets.IntSlider(
                                value=10,
                                min=10,
                                max=20,
                                step=1,
                                disabled=False,
                                continuous_update=False,
                                orientation='horizontal',
                                readout=True,
                                readout_format='d',
                                layout=slider_layout
                            ),
                'FiO2 %': widgets.IntSlider(
                                value=21,
                                min=21,
                                max=100,
                                step=1,
                                disabled=False,
                                continuous_update=False,
                                orientation='horizontal',
                                readout=True,
                                readout_format='d',
                                layout=slider_layout
                            ),
        }

        # construct the UI
        label_layout = widgets.Layout(width='150px', padding='0px 0px 0px 5px')
        labels = widgets.VBox([
            widgets.Label(l, layout=label_layout) for l in self.ventilator_ctrls.keys()
        ])
        
        self.change_ventilator = widgets.Button(
            description='Apply',
            disabled=True,
            button_style='',
            tooltip='Apply Ventilator',
            icon='',
            layout=widgets.Layout(margin='25px 0 10px 0')
        )
        self.change_ventilator.on_click(self.ventilator_action)
        update_ventilator = widgets.HBox(
            [
                self.change_ventilator
            ]
        )
        update_ventilator.layout.justify_content = 'center'

        settings = widgets.HBox([
            labels,
            widgets.VBox(
                list(self.ventilator_ctrls.values())
            )
        ])

        ventilator = widgets.VBox([
            vent_settings_heading,
            settings,
            update_ventilator
        ])
        ventilator.layout.border = '3px solid'

        return ventilator
   
    def start_pulse(self, b):
        # On first click start proccessing in our thread
        if self.waiting_to_start:
            self.setup_pulse = True
            self.waiting_to_start = False
            self.simulate.description = "Simulate"
        else:
            self.update_plots = not self.update_plots
            if self.update_plots:
                self.simulate.description = "Pause"
            else:
                self.simulate.description = "Simulate"

    def reset_pulse(self, b):
        self.waiting_to_start = True
        self.patient_a_ctrls['Compliance (mL/cmH20)'].disabled=False
        self.patient_a_ctrls['Resistance (cmH2O s/L)'].disabled=False
        self.patient_a_ctrls['Diffusion Impairment Factor'].disabled=False
        self.patient_b_ctrls['Compliance (mL/cmH20)'].disabled=False
        self.patient_b_ctrls['Resistance (cmH2O s/L)'].disabled=False
        self.patient_b_ctrls['Diffusion Impairment Factor'].disabled=False
        self.change_ventilator.disabled=True # Disable 'Apply' Button
        self.simulate.description = "Simulate"
        self.reset.disabled=True
        
    def change_plot(self, d):
        # Allow the user to change plots if we are paused
        if not self.update_plots and self.plot_selection.value is not self.y_ax.label:
            self.y_ax.label = self.plot_selection.value
            plot = self.plots[self.plot_selection.value]
            self.y_sc.min = plot.y_min-(abs(plot.y_min)*0.2)
            self.y_sc.max = plot.y_max+(abs(plot.y_max)*0.1)
            self.p0_line.x = self.time_s
            self.p0_line.y = plot.p0_y
            self.p1_line.x = self.time_s
            self.p1_line.y = plot.p1_y
    
    def ventilator_action(self, b):
        self.update_ventilator = True
        IERatio = self.ventilator_ctrls['I:E Ratio'].value
        totalPeriod_s = 60.0 / self.ventilator_ctrls['RespirationRate (bpm)'].value
        inspiratoryPeriod_s = IERatio * totalPeriod_s / (1 + IERatio)
        expiratoryPeriod_s = totalPeriod_s - inspiratoryPeriod_s;
        self.v_cfg.get_inspiration_machine_trigger_time().set_value(expiratoryPeriod_s, TimeUnit.s)
        self.v_cfg.get_expiration_cycle_time().set_value(inspiratoryPeriod_s, TimeUnit.s)
        self.v_cfg.get_peak_inspiratory_pressure().set_value(self.ventilator_ctrls['PIP (cmH2O)'].value, PressureUnit.mmHg)
        self.v_cfg.get_positive_end_expired_pressure().set_value(self.ventilator_ctrls['PEEP (cmH2O)'].value, PressureUnit.mmHg)
        self.v_cfg.get_fraction_inspired_gas("Oxygen").get_fraction_amount().set_value(self.ventilator_ctrls['FiO2 %'].value * 0.01)
        
    def pulse_loop(self):
        
        while self.running:
            if self.waiting_to_start:
                time.sleep(0.5)
                continue
            # Can only start a thread once, so put initial in here
            if self.setup_pulse:
                # Disable changing of patients
                self.patient_a_ctrls['Compliance (mL/cmH20)'].disabled=True
                self.patient_a_ctrls['Resistance (cmH2O s/L)'].disabled=True
                self.patient_a_ctrls['Diffusion Impairment Factor'].disabled=True
                self.patient_b_ctrls['Compliance (mL/cmH20)'].disabled=True
                self.patient_b_ctrls['Resistance (cmH2O s/L)'].disabled=True
                self.patient_b_ctrls['Diffusion Impairment Factor'].disabled=True
                self.change_ventilator.disabled=False # Turn on Apply button
                self.reset.disabled=False # allow a reset
                
                sim = SimulationData()
                sim.ID = 42
                sim.OutputBaseFilename = "./data/multiplex_ventilation/mv"
                # Use these ventilator settings
                sim.RespirationRate_Per_min = self.ventilator_ctrls['RespirationRate (bpm)'].value
                sim.IERatio = self.ventilator_ctrls['I:E Ratio'].value
                sim.PIP_cmH2O = self.ventilator_ctrls['PIP (cmH2O)'].value
                sim.PEEP_cmH2O = self.ventilator_ctrls['PEEP (cmH2O)'].value
                sim.FiO2 = self.ventilator_ctrls['FiO2 %'].value * 0.01
                # Add Patient 0
                p0 = sim.PatientComparisons.add().MultiplexVentilation
                p0.ID = 0
                p0.Compliance_mL_Per_cmH2O = self.patient_a_ctrls['Compliance (mL/cmH20)'].value
                p0.Resistance_cmH2O_s_Per_L = self.patient_a_ctrls['Resistance (cmH2O s/L)'].value
                p0.ImpairmentFraction = self.patient_b_ctrls['Diffusion Impairment Factor'].value
                # Add Patient 1
                p1 = sim.PatientComparisons.add().MultiplexVentilation
                p1.ID = 1
                p1.Compliance_mL_Per_cmH2O = self.patient_b_ctrls['Compliance (mL/cmH20)'].value
                p1.Resistance_cmH2O_s_Per_L = self.patient_a_ctrls['Resistance (cmH2O s/L)'].value
                p1.ImpairmentFraction = self.patient_b_ctrls['Diffusion Impairment Factor'].value
                
                self.pulse.destroy()
                if not self.pulse.create_engine(sim):
                    print("Error starting Pulse")
                    return
                draw = 0.0
                dt_s = 0.02
                draw_rate = 0.36
                self.setup_pulse = False
                self.update_plots = True
                
            if self.update_plots:
                if self.update_ventilator:
                    self.update_ventilator = False
                    self.pulse.process_action(self.ventilator)
                if not self.pulse.advance_time_s(dt_s, self.results):
                    print("Error running Pulse")
                    break;
                draw += dt_s
                # Update our time array
                self.time_s = np.append(self.time_s, self.time_s[-1]+dt_s)
                self.time_s = np.delete(self.time_s, 0)
                # Update our value arrays
                p0 = self.results.PatientComparisons[0].MultiplexVentilation
                p1 = self.results.PatientComparisons[1].MultiplexVentilation
                self.plots["AirwayFlow_L_Per_min"].update(p0.AirwayFlow_L_Per_min,p1.AirwayFlow_L_Per_min)
                self.plots["AirwayPressure_cmH2O"].update(p0.AirwayPressure_cmH2O,p1.AirwayPressure_cmH2O)
                self.plots["ArterialCarbonDioxidePartialPressure_mmHg"].update(p0.ArterialCarbonDioxidePartialPressure_mmHg,p1.ArterialCarbonDioxidePartialPressure_mmHg)
                self.plots["ArterialOxygenPartialPressure_mmHg"].update(p0.ArterialOxygenPartialPressure_mmHg,p1.ArterialOxygenPartialPressure_mmHg)
                self.plots["HorowitzIndex_mmHg"].update(p0.HorowitzIndex_mmHg,p1.HorowitzIndex_mmHg)
                self.plots["EndTidalCarbonDioxidePressure_mmHg"].update(p0.EndTidalCarbonDioxidePressure_mmHg,p1.EndTidalCarbonDioxidePressure_mmHg)
                self.plots["MeanAirwayPressure_cmH2O"].update(p0.MeanAirwayPressure_cmH2O,p1.MeanAirwayPressure_cmH2O)
                self.plots["OxygenationIndex"].update(p0.OxygenationIndex,p1.OxygenationIndex)
                self.plots["OxygenSaturation"].update(p0.OxygenSaturation,p1.OxygenSaturation)
                self.plots["OxygenSaturationIndex_mmHg"].update(p0.OxygenSaturationIndex_mmHg,p1.OxygenSaturationIndex_mmHg)
                self.plots["SFRatio"].update(p0.SFRatio,p1.SFRatio)
                self.plots["TidalVolume_mL"].update(p0.TidalVolume_mL,p1.TidalVolume_mL)
                self.plots["TotalLungVolume_mL"].update(p0.TotalLungVolume_mL,p1.TotalLungVolume_mL)
                # Show who is active
                if self.plot_selection.value is not self.y_ax.label:
                    draw = draw_rate # Drop down changed, force a redraw
                if draw >= draw_rate:
                    draw = 0.0
                    plot = self.plots[self.plot_selection.value]
                    with self.fig.hold_sync():
                        self.y_ax.label = self.plot_selection.value
                        self.y_sc.min = plot.y_min-(abs(plot.y_min)*0.35)
                        self.y_sc.max = plot.y_max+(abs(plot.y_max)*0.25)
                        self.p0_line.x = self.time_s
                        self.p0_line.y = plot.p0_y
                        self.p1_line.x = self.time_s
                        self.p1_line.y = plot.p1_y
                #time.sleep(0.02)
            else:
                time.sleep(0.5)
        # Ya'll don't come back now, yah hear!
        # You cannot restart a thread in python!
        self.pulse.destroy()

if __name__ == '__main__':
    app = MVSimExplorer()
    app.run()