# Pulse Jupyter Notebooks

Examples that illustrate how to use Pulse via Jupyter notebooks in Python

All the notebooks can be run interactively in the web browser via Binder by clicking "Launch binder".

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kitware.com%2Fphysiology%2Fjupyter.git/master)

## Docker Container

We provide docker containes for each release of the Pulse engine.

Versioned containers can be downloaded from our [docker hub organization](https://hub.docker.com/r/kitware/pulse/tags)

Docker contianers associated with this repository are prefixed with `jupyter_`

## Local Building/Hosting

The DockerFile in the repository can also be used to host the Jupyter system locally.  This process is fairly simple:

1. Clone/download this repository.

2. Build a Docker image

   Execute the ```docker build``` command in the repository.  The resultant image should be tagged with the ```-t``` flag, as we will use it later.  The tag is simply an arbitrary string which is used as the image name.

    ```
        $ docker build -t <tag_name> .
    ```

3. Run The Built Docker Image

   Next, we execute the ```docker run``` comand with the name of the image above.  The ```-p``` argument is used to forward the specified port of the container to a port on the host machine. We will also pass in the ```jupyter``` arguments needed to start the Notebook.

   ```
       docker run -p 8888:8888 <tag_name> jupyter notebook --ip="0.0.0.0"
   ```

4.  Access The Notebook Index

    As the Docker container starts, it will print a message that the Notebook Server can be accessed by visting a website with a particular token.  This ```127.0.0.1``` URL should be copy/pasted into a web browser to open the front page of the Notebook .

---

Below is output from running these commands.  The URL to copy is found in two places: just after the ```docker run``` command starts and the final line of the print out:

   ```
        user@RANNOCH MINGW64 ~/work/Medical/jupyter
        $ docker build -t pulse_jupyter .
        Sending build context to Docker daemon  376.8kB
        Step 1/27 : FROM kitware/pulse:3.0.0
        ---> e76573e7691a
        Step 2/27 : ARG NB_USER=notebooks
        ...
        Step 27/27 : USER ${NB_USER}
        ---> Running in bff013eb5512
        Removing intermediate container bff013eb5512
        ---> b8bbb668cd3f
        Successfully built b8bbb668cd3f
        Successfully tagged pulse_jupyter:latest

        user@RANNOCH MINGW64 ~/work/Medical/jupyter
        $ docker run -p 8888:8888 pulse_jupyter jupyter notebook --ip="0.0.0.0"
        [I 16:19:00.911 NotebookApp] Writing notebook server cookie secret to /home/notebooks/.local/share/jupyter/runtime/notebook_cookie_secret
        [I 16:19:01.151 NotebookApp] Serving notebooks from local directory: /home/notebooks
        [I 16:19:01.151 NotebookApp] The Jupyter Notebook is running at:
        [I 16:19:01.151 NotebookApp] http://f6deb217dd4d:8888/?token=11f920996bec923c23701c65edc86fe2b665d27d31d82b04
        [I 16:19:01.151 NotebookApp]  or http://127.0.0.1:8888/?token=11f920996bec923c23701c65edc86fe2b665d27d31d82b04
        [I 16:19:01.151 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
        [W 16:19:01.156 NotebookApp] No web browser found: could not locate runnable browser.
        [C 16:19:01.156 NotebookApp]

            To access the notebook, open this file in a browser:
                file:///home/notebooks/.local/share/jupyter/runtime/nbserver-1-open.html
            Or copy and paste one of these URLs:
                http://f6deb217dd4d:8888/?token=11f920996bec923c23701c65edc86fe2b665d27d31d82b04
            or http://127.0.0.1:8888/?token=11f920996bec923c23701c65edc86fe2b665d27d31d82b04
   ```
